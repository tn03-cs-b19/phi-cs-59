#include <iostream>
#include <string.h>
#include "tinyxml.h"

using std::string;
using namespace std;

typedef struct
{
	int x;
	int y;
	int w;
	int h;
} UiRect_t;

int main()
{
	UiRect_t ur[20];
	int count = 0;
	TiXmlDocument* myDocument = new TiXmlDocument();
	myDocument->LoadFile("Rectangle.xml");
	TiXmlElement* rootElement = myDocument->RootElement();		//uirects
	TiXmlElement* RectElement = rootElement->FirstChildElement();   //rect
	while (RectElement)
	{
		TiXmlAttribute* attributeOfRect = RectElement->FirstAttribute();		//get x attribute		 
		for (int i = 0; i < 20; i++)
		{
			while (attributeOfRect)
			{
				std::cout << attributeOfRect->Name() << " : " << attributeOfRect->Value() << std::endl;
				if ((char)attributeOfRect->Name()=='x')
				{
					ur[i].x = (int)attributeOfRect->Value();
				}
				else if ((char)attributeOfRect->Name() == 'y')
				{
					ur[i].y = (int)attributeOfRect->Value();
				}
				else if ((char)attributeOfRect->Name() == 'w')
				{
					ur[i].w = (int)attributeOfRect->Value();
				}
				else 
				{
					ur[i].h = (int)attributeOfRect->Value();
				}
				attributeOfRect = attributeOfRect->Next();
			}
		}
		RectElement = RectElement->NextSiblingElement();
	}
	return 0;
}